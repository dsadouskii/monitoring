import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import Resource from 'vue-resource'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.config.productionTip = false

Vue.use(Resource);
Vue.use(BootstrapVue);


Vue.mixin({
  methods: {
    animateLabel: function(e){
      let prevElemStyle = e.target.previousElementSibling.style;
      prevElemStyle.top = 5+'px';
      prevElemStyle.fontSize = 12+'px';
      prevElemStyle.color = 'var(--main-greyFull-color)';
    },
    animateLabelReverse: function(e){
      if (e.target.value === '') {
        let prevElemStyle = e.target.previousElementSibling.style;
        prevElemStyle.top = 'unset';
        prevElemStyle.fontSize = 14+'px';
        prevElemStyle.color = 'var(--main-grey-color)';
      }
    },
    validateEmail:(email)=>{
      if (String(email).length < 5){
        return true
      }
      return String(email).match(/[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}/i);
    },
    getFileSize(size) {
      var i = Math.floor( Math.log(size) / Math.log(1024) );
      return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
