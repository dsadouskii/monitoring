import Vue from 'vue'
import Router from 'vue-router'
import Landing from './views/Landing.vue'
const Login  = ()=> import( /*webpackChunkName: "Login"*/ './views/Login.vue')
const Registration  = ()=> import( /*webpackChunkName: "Registration"*/ './views/Registration.vue')
const Restore  = ()=> import( /*webpackChunkName: "Restore"*/ './views/Restore.vue')
const Cabinet  = ()=> import( /*webpackChunkName: "Cabinet"*/ './views/Cabinet.vue')
const PersonalCabinet  = ()=> import( /*webpackChunkName: "PersonalCabinet"*/ './views/cabinetPages/PersonalCabinet.vue')
const MySettings  = ()=> import( /*webpackChunkName: "MySettings"*/ './views/cabinetPages/MySettings.vue')
const Workgroup  = ()=> import( /*webpackChunkName: "Workgroup"*/ './views/cabinetPages/Workgroup.vue')
const Notifications  = ()=> import( /*webpackChunkName: "Notifications"*/ './views/cabinetPages/Notifications.vue')
const AccessPay  = ()=> import( /*webpackChunkName: "AccessPay"*/ './views/cabinetPages/AccessPay.vue')
const EndedDocs  = ()=> import( /*webpackChunkName: "EndedDocs"*/ './views/cabinetPages/EndedDocs.vue')

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'landing',
      component: Landing
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/registration',
      name: 'registration',
      component: Registration
    },
    {
      path: '/restore',
      name: 'restore',
      component: Restore
    },
    {
      path: '/cabinet',
      name: 'cabinet',
      component: Cabinet,
      children:[
        {
          path: 'personal_cabinet',
          name: 'personal_cabinet',
          component: PersonalCabinet,
          children: [
            {
              path: 'settings',
              name: 'settings',
              component: MySettings
            },
            {
              path: 'workgroup',
              name: 'workgroup',
              component: Workgroup
            },
            {
              path: 'notifications',
              name: 'notifications',
              component: Notifications
            },
            {
              path: 'access_pay',
              name: 'access_pay',
              component: AccessPay
            },
            {
              path: 'ended_docs',
              name: 'ended_docs',
              component: EndedDocs
            }
          ]
        }
      ]
    }
  ]
})
